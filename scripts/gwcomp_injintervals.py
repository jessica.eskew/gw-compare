#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Return the interquartile and 90% ranges on the overlap between point-estimate
waveforms and injections.  Typically, the point-estimate is the median BayesWave
reconstruction, but all that is required is a list of overlap overlaps.
"""

import sys, os
import numpy as np

import argparse


def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--input-filename", default=None, type=str, required=True)
    parser.add_argument("--overlaps-in-col", default=0, type=int, help="compute\
    intervals for overlaps stored in this column of input-filename")
    parser.add_argument("--output-filename", default=None, type=str)
    parser.add_argument("--map-snr", default=12.6760132186, type=float)
    parser.add_argument("--make-plots", default=False, action="store_true")
    parser.add_argument("--save-for-publication", default=False, action="store_true")

    opts = parser.parse_args()

    return opts


#
# Load data
#
opts = parser()
overlaps = np.loadtxt(opts.input_filename)[:,opts.overlaps_in_col]


#
# Compute intervals for specified column of data
# 
percentiles = np.percentile(overlaps, [5, 25, 50, 75, 95])
print >> sys.stdout, "[5, 25, 50, 75, 95] percentiles:"
print >> sys.stdout, "{}".format(percentiles)
if opts.save_for_publication:
    if opts.output_filename is None:
        opts.output_filename = opts.input_filename.replace('.txt',
                '-percentiles.txt')
    f = open(opts.output_filename,'w')
    f.writelines("# [5, 25, 50, 75, 95] percentiles\n")
    for p in percentiles:
        f.writelines("{} ".format(p))
    f.writelines("\n")
    f.close()



