#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Construct publication-quality Q-scans.

Use of gwpy allows us to read data directly using NDS2.  Also supports frame
cache files for local frames.

Note axes for plots are limited to values appropriate for GW170104.
"""

import sys, os
import argparse
import numpy as np
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from gwpy.timeseries import TimeSeries
from gwpy.signal import filter_design

def two_floats(value):
    values = value.split()
    if len(values) != 2:
        raise argparse.ArgumentError
    values = map(float, values)
    return values


def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--H1-channel", type=str,
            default="H1:DCS-CALIB_STRAIN_C01")
    parser.add_argument("--L1-channel", type=str,
            default="L1:DCS-CALIB_STRAIN_C01")
    parser.add_argument("--trigtime", type=float, default=1167559936.6)
    parser.add_argument("--datalen", type=float, default=2.)
    parser.add_argument("--plotwin", type=float, default=0.25)
    parser.add_argument("--f-min", type=float, default=10)
    parser.add_argument("--f-max", type=float, default=1024)
    parser.add_argument("--searchwin", type=float, default=0.5)
    parser.add_argument("--linearfreqs", default=False, action="store_true")
    parser.add_argument("--H1-cache", default=None)
    parser.add_argument("--L1-cache", default=None)
    parser.add_argument("--output-path", default="./")
    parser.add_argument("--snr-max", default=25, type=float)
    parser.add_argument("--xlims", default="0.425 0.625",
            help="x-axis limits for main panel", type=two_floats)
    parser.add_argument("--ylims", default="-1 1",
            help="x-axis limits for main panel", type=two_floats)
    parser.add_argument("--Lshift", default=3e-3, type=float,
            help="L1 time shift to align with H1")
    parser.add_argument("--passband", default=[35, 350], nargs=2,
            help="Band pass the data through these frequencies")
    parser.add_argument("--fftlen", default=0.5,
            help="FFTlen argument for whitening PSD")
    parser.add_argument("--overlap", default=0.25,
            help="overlap argument for whitening PSD")
    parser.add_argument("--h1-predicted", default=None,
            help="ascii file with time,strain for a predicted waveform")
    parser.add_argument("--l1-predicted", default=None,
            help="ascii file with time,strain for a predicted waveform")

    opts = parser.parse_args()

    return opts

ifos = ['H1', 'L1']
opts=parser()
caches = [opts.H1_cache, opts.L1_cache]
channels = [opts.H1_channel, opts.L1_channel]

passband = [ float(v) for v in opts.passband ]

#
# Retrieve Data
#
datastart=opts.trigtime-0.5*opts.datalen
datastop=opts.trigtime+0.5*opts.datalen
data = []
for i,(ifo,cache) in enumerate(zip(ifos, caches)):
    if cache is None:
        print "fetching {} over NDS2".format(channels[i])
        data.append(TimeSeries.fetch("{}".format(channels[i]),
            datastart, datastop, verbose=True))
    else:
        print "reading {0} from {1}".format(channels[i], cache)
        data.append(TimeSeries.read(cache, "{}".format(channels[i]),
            datastart, datastop))

#
# Filter (bandpass and notches)
#
print "Filtering"
import filt

filterBas = filt.get_filt_coefs(data[0].sample_rate.value, min(passband),
        max(passband), False, False)
filtered_data = [TimeSeries(filt.filter_data(d.value, filterBas)) for d in data
        ]


#
# Make Qtransforms
#
print "Making Q-scans"
winstart=opts.trigtime-0.5*opts.plotwin
winstop=opts.trigtime+0.5*opts.plotwin

qscans = [d.q_transform(frange=(opts.f_min,opts.f_max), qrange=(5.5,5.5),
    gps=opts.trigtime, search=opts.searchwin, outseg=(winstart,winstop),
    method="median-mean", fftlength=opts.fftlen, overlap=opts.overlap) for d
    in data]

#
# Plotting
#
print "plotting"

#plt.style.use("ggplot")
fig_width_pt = 246.0  # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0/72.27               # Convert pt to inches
golden_mean = (np.sqrt(5)-1.0)/2.0         # Aesthetic ratio
fig_width = fig_width_pt*inches_per_pt  # width in inches
fig_height =fig_width*golden_mean       # height in inches
fig_size = [fig_width,fig_height]
params = {
          'axes.labelsize': 8,
          'font.size': 8,
          'legend.fontsize': 8,
          'xtick.color': 'k',
          'xtick.labelsize': 8,
          'ytick.color': 'k',
          'ytick.labelsize': 8,
          'text.usetex': True,
          'text.color': 'k',
          'figure.figsize': fig_size
          }
import pylab
pylab.rcParams.update(params)


# Adjust times so that zero is on an integer second
trigtime,delta = divmod(opts.trigtime,1)

times = np.arange(qscans[0].span[0],qscans[0].span[1],qscans[0].dt.value) - \
        opts.trigtime
times += delta

delta_t=data[0].dx.value
local_times = data[0].x0.value \
    + np.arange(0, len(data[0])*delta_t, delta_t)\
    - opts.trigtime
local_times += delta
import subprocess
command = ['lalapps_tconvert', str(int(trigtime))]
p = subprocess.Popen(command, stdout=subprocess.PIPE)
timestr = p.stdout.read().replace('\n','')

#longnames = ["LIGO Hanford", "LIGO Livingston"]
longnames = ["Hanford", "Livingston"]
#longnames = ["H", "L"]

# Frequency axis
freqs = np.arange(opts.f_min, opts.f_max, qscans[0].df.value)


# Q-scan only axes
fqscans, axqscans = plt.subplots(nrows=2,
        figsize=(fig_size[0],1.5*fig_size[1]), dpi=500)


# Time-series only axies
ftime, axtime = plt.subplots(nrows=2, figsize=fig_size, dpi=500, gridspec_kw
        = {'height_ratios':[1, 0.5]})

# Complete axes
fshared, axshared = plt.subplots(nrows=4, figsize=(fig_size[0],
    2.5*fig_size[1]), sharex=False, dpi=500, gridspec_kw = {'height_ratios':[1, 1,
        1, 0.5]})


for i in xrange(len(ifos)):

    # --- Q-scan
    yticks=np.arange(np.ceil(np.log2(freqs.min())),
            np.ceil(np.log2(freqs.max())), 1)


    # --- Time Series
    if i==1:
        # Flip L1 by -1 and shift back Lshift ms
        p=axshared[i].pcolormesh(times-opts.Lshift, np.log2(freqs),
                np.sqrt(qscans[i].T), vmin=0, vmax=opts.snr_max,
                cmap='viridis',shading='gouraud', rasterized=True)

        axshared[2].plot(local_times-opts.Lshift, 
                    -1*filtered_data[i]*1e21, 
                    label=r'$\mathrm{%s}$'%longnames[i], 
                    alpha=1.0, linewidth=1.0, color=u'#1f77b4')


        # Flip L1 by -1 and shift back Lshift ms
        pq=axqscans[i].pcolormesh(times-opts.Lshift, np.log2(freqs),
                np.sqrt(qscans[i].T), vmin=0, vmax=opts.snr_max,
                cmap='viridis',shading='gouraud', rasterized=True)

        axtime[0].plot(local_times-opts.Lshift, 
                    -1*filtered_data[i]*1e21, 
                    label=r'$\mathrm{%s}$'%longnames[i], 
                    alpha=1.0, linewidth=1.0, color=u'#1f77b4')


    else:
        pq=axshared[i].pcolormesh(times, np.log2(freqs),
                np.sqrt(qscans[i].T), vmin=0, vmax=opts.snr_max,
                cmap='viridis',shading='gouraud', rasterized=True)
        axshared[2].plot(local_times, filtered_data[i]*1e21, 
                    label=r'$\mathrm{%s}$'%longnames[i], 
                    alpha=1.0, linewidth=1.0, color=u'#ff7f0e')

        p=axqscans[i].pcolormesh(times, np.log2(freqs),
                np.sqrt(qscans[i].T), vmin=0, vmax=opts.snr_max,
                cmap='viridis',shading='gouraud', rasterized=True)

        axtime[0].plot(local_times, 
                    filtered_data[i]*1e21, 
                    label=r'$\mathrm{%s}$'%longnames[i], 
                    alpha=1.0, linewidth=1.0, color=u'#ff7f0e')

    axshared[i].set_yticks(yticks)
    axshared[i].set_yticklabels(["$%s$"%str(int(2**l)) for l in yticks])
    axshared[i].set_xlim(opts.xlims)
    axshared[i].minorticks_on()

    axqscans[i].set_yticks(yticks)
    axqscans[i].set_yticklabels(["$%s$"%str(int(2**l)) for l in yticks])
    axqscans[i].set_xlim(opts.xlims)
    axqscans[i].minorticks_on()



axshared[2].set_xlim(opts.xlims)
yticks=np.arange(int(min(opts.ylims)), int(max(opts.ylims))+0.5, 0.5)
axshared[2].set_yticks(yticks)
axshared[2].set_ylim(opts.ylims)
axshared[2].minorticks_on()

axtime[0].set_xlim(opts.xlims)
yticks=np.arange(int(min(opts.ylims)), int(max(opts.ylims))+0.5, 0.5)
axtime[0].set_yticks(yticks)
axtime[0].set_ylim(opts.ylims)
axtime[0].minorticks_on()


# Full-figure
fshared.subplots_adjust(top=0.85,bottom=0.125,hspace=0.07,left=0.18,right=0.95)
cbar_ax = fshared.add_axes([0.18, 0.865, 0.77, 0.03])

labelstr=r"$\textrm{Frequency [Hz]}$"
fshared.text(0.035, 0.66, labelstr, va='center', 
             rotation='vertical')
axshared[2].set_ylabel(r'$\textrm{Strain }[10^{-21}]$', color='k')

cbar=fshared.colorbar(p, cax=cbar_ax, orientation='horizontal')
cbar.set_label(r'$\textrm{Normalized Amplitude}$', color='k')
cbar.set_clim(0,opts.snr_max)
cbar.ax.xaxis.set_ticks_position('top')
cbar.ax.xaxis.set_label_position('top')

# Q-scans-only
fqscans.subplots_adjust(top=0.85,bottom=0.125,hspace=0.07,left=0.18,right=0.95)
qscans_cbar_ax = fqscans.add_axes([0.18, 0.865, 0.77, 0.03])


qscans_cbar=fqscans.colorbar(pq, cax=qscans_cbar_ax, orientation='horizontal')
qscans_cbar.set_label(r'$\textrm{Normalized Amplitude}$', color='k')
qscans_cbar.set_clim(0,opts.snr_max)
qscans_cbar.ax.xaxis.set_ticks_position('top')
qscans_cbar.ax.xaxis.set_label_position('top')


for axis in ['top','bottom','left','right']:
              axshared[0].spines[axis].set_linewidth(0.25)
              axshared[1].spines[axis].set_linewidth(0.25)
              axshared[2].spines[axis].set_linewidth(0.25)
              cbar_ax.spines[axis].set_linewidth(0.25)

              axqscans[0].spines[axis].set_linewidth(0.25)
              axqscans[1].spines[axis].set_linewidth(0.25)
              cbar_ax.spines[axis].set_linewidth(0.25)

              axtime[0].spines[axis].set_linewidth(0.25)
              axtime[1].spines[axis].set_linewidth(0.25)

ticks=axshared[1].get_xticks()
axshared[0].set_xticks([])
axshared[1].set_xticks([])

axqscans[0].set_xticks([])
axtime[0].set_xticks([])


fqscans.subplots_adjust(top=0.85,bottom=0.125,hspace=0.07,left=0.18,right=0.95)
ftime.subplots_adjust(top=0.95,bottom=0.2,hspace=0.07,left=0.18,right=0.95)
qscan_cbar_ax = fqscans.add_axes([0.18, 0.865, 0.77, 0.03])

labelstr=r"$\textrm{Frequency [Hz]}$"
fqscans.text(0.035, 0.5, labelstr, va='center', 
             rotation='vertical')
qscan_cbar=fqscans.colorbar(p, cax=cbar_ax, orientation='horizontal')
qscan_cbar.set_label(r'$\textrm{Normalized Amplitude}$', color='k')
qscan_cbar.set_clim(0,opts.snr_max)
qscan_cbar.ax.xaxis.set_ticks_position('top')
qscan_cbar.ax.xaxis.set_label_position('top')


for axis in ['top','bottom','left','right']:
              axshared[0].spines[axis].set_linewidth(0.25)
              axshared[1].spines[axis].set_linewidth(0.25)
              axshared[2].spines[axis].set_linewidth(0.25)
              cbar_ax.spines[axis].set_linewidth(0.25)

              axqscans[0].spines[axis].set_linewidth(0.25)
              axqscans[1].spines[axis].set_linewidth(0.25)
              qscans_cbar_ax.spines[axis].set_linewidth(0.25)

              axtime[0].spines[axis].set_linewidth(0.25)
              axtime[1].spines[axis].set_linewidth(0.25)

ticks=axshared[1].get_xticks()
axshared[0].set_xticks([])
axshared[1].set_xticks([])

axqscans[0].set_xticks([])
axtime[0].set_xticks([])


fshared.text(0.2, 0.825, r"$\textrm{Hanford}$", 
                     va='center', rotation='horizontal',
                     color='white')
fshared.text(0.2, 0.62, r"$\textrm{Livingston}$", 
                     va='center', rotation='horizontal',
                     color='white')

fqscans.text(0.2, 0.8, r"$\textrm{Hanford}$", 
                     va='center', rotation='horizontal',
                     color='white')
fqscans.text(0.2, 0.42, r"$\textrm{Livingston}$", 
                     va='center', rotation='horizontal',
                     color='white')

# Time / residuals stuff
for axis in ['top','bottom','left','right']:
    axshared[3].spines[axis].set_linewidth(0.25)

axshared[2].set_xticks([])
axshared[3].minorticks_on()
axshared[3].set_xlabel(r'$\textrm{Time from %s [s]}$'%timestr, color='k')


axqscans[1].set_xlabel(r'$\textrm{Time from %s [s]}$'%timestr, color='k')
axtime[0].minorticks_on()
axtime[1].minorticks_on()
axtime[0].set_ylabel(r'$\textrm{Strain }[10^{-21}]$', color='k')

axtime[1].set_xlabel(r'$\textrm{Time from %s [s]}$'%timestr, color='k')

recdata = [np.loadtxt(opts.h1_predicted), np.loadtxt(opts.l1_predicted)]

times = recdata[0][:,0]
times -= opts.trigtime
times += delta
maxL_h1 = recdata[0][:,1]
maxL_l1 = recdata[1][:,1]

# demo waveform (might) have lower sample rate
delta_t = np.diff(times)[0]
filterBas = filt.get_filt_coefs(1/delta_t, min(passband),
        max(passband), False, False)
filtered_h1_signal = filt.filter_data(maxL_h1, filterBas)
filtered_l1_signal = filt.filter_data(maxL_l1, filterBas)

axshared[2].plot(times, filtered_h1_signal*1e21,
        label=r'$\textrm{Model}$', alpha=1.0,
        linewidth=1.0, color='k')
axtime[0].plot(times, filtered_h1_signal*1e21,
        label=r'$\textrm{Model}$', alpha=1.0,
        linewidth=1.0, color='k')

axshared[2].legend(loc=8,
                   ncol=3, mode="expand", borderaxespad=0., frameon=False)
axtime[0].legend(loc=8,
                   ncol=3, mode="expand", borderaxespad=0., frameon=False)

# Residuals
startidx = np.argwhere(local_times>=times[0])[0][0]
for f,(fd,maxL) in enumerate(zip(filtered_data,[filtered_h1_signal,filtered_l1_signal])):
    residual = fd[startidx:startidx+len(maxL)] - maxL
    if f==1:
        axshared[3].plot(times-opts.Lshift, -1*1e21*residual, linewidth=1.0,
                color=u'#1f77b4')
        axtime[1].plot(times-opts.Lshift, -1*1e21*residual, linewidth=1.0,
                color=u'#1f77b4')
    else:
        axshared[3].plot(times, 1e21*residual, linewidth=1.0, color=u'#ff7f0e')
        axtime[1].plot(times, 1e21*residual, linewidth=1.0, color=u'#ff7f0e')
axshared[3].set_xlim(opts.xlims)
axshared[3].set_ylabel(r"$\textrm{Residual}$")
axtime[1].set_xlim(opts.xlims)
axtime[1].set_ylabel(r"$\textrm{Residual}$")

# Save filtered strains to text file
header = "Time H1_strain_data H1_strain_data_filtered H1_strain_model H1_strain_model_filtered"
header += " L1_strain_data L1_strain_data_filtered L1_strain_model L1_strain_model_filtered"

data_idx = np.concatenate(np.argwhere( (local_times>=min(opts.xlims)) *
        (local_times<=max(opts.xlims))))
model_idx = np.concatenate(np.argwhere( (times>=min(opts.xlims)) *
        (times<=max(opts.xlims))))
if len(data_idx)>len(model_idx):data_idx=data_idx[1:]
if len(data_idx)<len(model_idx):model_idx=model_idx[1:]

savearr=np.array([local_times[data_idx], data[0].value[data_idx],
    filtered_data[0].value[data_idx], maxL_h1[model_idx],
    filtered_h1_signal[model_idx], data[1].value[data_idx],
    filtered_data[1].value[data_idx], maxL_l1[model_idx],
    filtered_l1_signal[model_idx]])

fname=os.path.join(opts.output_path,
        'H1L1-{hchannel}-{lchannel}-qscan-strainseries.txt'.format(
            hchannel=opts.H1_channel, lchannel=opts.L1_channel))
#np.savetxt(fname, savearr.T, header=header)


fshared.savefig(os.path.join(opts.output_path,
    'H1L1-{hchannel}-{lchannel}-qscan.png'.format(
        ifo=ifos[i], hchannel=opts.H1_channel, lchannel=opts.L1_channel)),
    transparent=True)
fshared.savefig(os.path.join(opts.output_path,
    'H1L1-{hchannel}-{lchannel}-qscan.pdf'.format(
        ifo=ifos[i], hchannel=opts.H1_channel, lchannel=opts.L1_channel)),
    transparent=True)

fqscans.savefig(os.path.join(opts.output_path,
    'H1L1-{hchannel}-{lchannel}-qscan_only.png'.format(
        ifo=ifos[i], hchannel=opts.H1_channel, lchannel=opts.L1_channel)),
    transparent=True)
fqscans.savefig(os.path.join(opts.output_path,
    'H1L1-{hchannel}-{lchannel}-qscan_only.pdf'.format(
        ifo=ifos[i], hchannel=opts.H1_channel, lchannel=opts.L1_channel)),
    transparent=True)

ftime.savefig(os.path.join(opts.output_path,
    'H1L1-{hchannel}-{lchannel}-time_only.png'.format(
        ifo=ifos[i], hchannel=opts.H1_channel, lchannel=opts.L1_channel)),
    transparent=True)
ftime.savefig(os.path.join(opts.output_path,
    'H1L1-{hchannel}-{lchannel}-time_only.pdf'.format(
        ifo=ifos[i], hchannel=opts.H1_channel, lchannel=opts.L1_channel)),
    transparent=True)




