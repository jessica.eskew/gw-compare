#!/home/sudarshan.ghonge/virtualenvs/gw-compare/bin/python
#
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#               2017-2020 Sudarshan Ghonge <sudarshan.ghonge@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Perform the main processing for waveform reconstruction comparisons.

This script computes overlaps and residuals between burst reconstructions and
LALInference reconstructions.   LALInference reconstructions are obtained by
loading ascii files produced using gwcomp_reclal.py.

Optional data products include a pickle containing the main summary statistics
and data required for publication figures and results, produced using
gwcomp_writepaper.py

"""

import sys, os
import glob
import numpy as np

import argparse

import pycbc.types
import pycbc.filter



def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--li-waveforms", type=str, default=None)
    parser.add_argument("--cwb-waveforms", type=str, default=None, nargs='+')
    parser.add_argument("--li-summary-waveforms", nargs='+', type=str, default=None)
    parser.add_argument("--injections", default=False, action="store_true")
    parser.add_argument("--cwb-injections", default=False, action="store_true")
    parser.add_argument("--bw-dir", type=str, default=None, required=True)
    parser.add_argument("--srate", type=int, default=2048)
    parser.add_argument("--title", type=str, default=None)
    parser.add_argument("--bw-epoch", type=float, default=None)
    parser.add_argument("--li-epoch", type=float, default=None)
    parser.add_argument("--trigtime", type=float, default=1167559936.6)
    parser.add_argument("--nwaves", type=int, default=None)
    parser.add_argument("--flow", type=float, default=20.)
    parser.add_argument("--output-dir", default=None, type=str, required=True)
    parser.add_argument("--save-for-publication", default=False, action="store_true")
    parser.add_argument("--nr-dir", default=None, type=str)
    parser.add_argument("--duration", default=4.0, type=float)

    opts = parser.parse_args()

    return opts


def network_overlap(ts1_list, ts2_list, f_low=20.0):
    """
    Compute network overlap
    """
    overlap_sum=0.
    overlap_sum = sum([pycbc.filter.match(ts1, ts2,
        low_frequency_cutoff=f_low)[0]*pycbc.filter.sigma(ts1,low_frequency_cutoff=f_low)*pycbc.filter.sigma(ts2, low_frequency_cutoff=f_low)
        for ts1,ts2 in zip(ts1_list,ts2_list)])
    norm1 = sum([pycbc.filter.sigmasq(ts1,low_frequency_cutoff=f_low) for ts1 in
        ts1_list])
    norm2 = sum([pycbc.filter.sigmasq(ts2, low_frequency_cutoff=f_low) for ts2 in
        ts2_list])

    net_overlap = overlap_sum / np.sqrt(norm1*norm2)

    return net_overlap

# --------------------------------------------------------------------------------
#
# Input
#

opts = parser()

if(len(opts.li_summary_waveforms)==2):
  ifos = ['H1', 'L1']
elif(len(opts.li_summary_waveforms)==3):
  ifos = ['H1','L1','V1']
else:
  print "Provide either 2 or 3 detector waveforms"
  sys.exit(0)

if(opts.cwb_waveforms is not None):
  if(len(opts.li_summary_waveforms)!=len(opts.cwb_waveforms)):
    print "Number of LI and cWB waveform arguments not the same"
    sys.exit(0)


# --------------------------------------------------------------------------------
#
# Whitened Strain data
#

# We'll save this later

if(os.path.exists(os.path.join(opts.bw_dir, 'post/whitened_data.dat.0'))):
  white_data_fmt = os.path.join(opts.bw_dir, 'post/whitened_data.dat.{}')
  infiles = [white_data_fmt.format(ifo) for ifo in xrange(len(ifos))]
else:
  white_data_fmt = os.path.join(opts.bw_dir, 'post/whitened_data_{}.dat')
  infiles = [white_data_fmt.format(ifos[ifo]) for ifo in xrange(len(ifos))]
  
  
#white_data_fmt = os.path.join(opts.bw_dir, 'post/whitened_data.dat.{}')
white_data = [np.loadtxt(infile) for infile in infiles]

# --------------------------------------------------------------------------------
#
# BayesWave (bw)
#
if opts.cwb_waveforms is None:
    
    if(os.path.exists(os.path.join(opts.bw_dir,'post/signal_recovered_whitened_waveform.dat.0'))):
      bw_infile_fmt = os.path.join(opts.bw_dir,
            'post/signal_recovered_whitened_waveform.dat.{}')
      bw_infiles = [bw_infile_fmt.format(ifo) for ifo in xrange(len(ifos))]
    else:
      bw_infile_fmt = os.path.join(opts.bw_dir,
            'post/signal/signal_recovered_whitened_waveform_{}.dat')
      bw_infiles = [bw_infile_fmt.format(ifos[ifo]) for ifo in xrange(len(ifos))]
      
    #bw_infile_fmt = os.path.join(opts.bw_dir,
    #        'post/signal_recovered_whitened_waveform.dat.{}')
    print "Loading bayeswave"
    bw_waves = np.array([np.loadtxt(infile) for infile in bw_infiles])
    if opts.nwaves is not None:
      nwaves = opts.nwaves
      idx = np.random.permutation(len(bw_waves[0]))
      bw_waves = [l[idx][:opts.nwaves,:] for l in bw_waves]

    # Set epochs
    if opts.bw_epoch is None:
        opts.bw_epoch = opts.trigtime - 0.5*opts.duration
    if opts.li_epoch is None:
        opts.li_epoch = opts.trigtime - 0.5*opts.duration


    # Get credible intervals for reconstructed T-domain waveforms
    bw_intervals=[]
    for i in xrange(len(ifos)):
        bw_intervals.append([pycbc.types.TimeSeries(bdata, delta_t=1./opts.srate,
            epoch=opts.bw_epoch) for bdata in np.percentile(bw_waves[i], [5, 50, 95],
                axis=0)])

    # Data - Reconstruction Residuals
    pe_burst_residuals = [wd-bw_interval[1].data for wd, bw_interval in
            zip(white_data, bw_intervals)]
    burst_residuals = [wd - bw for wd, bw in zip(white_data, bw_waves)]

    burst_residuals_intervals = [np.percentile(res, [5, 50, 95], axis=0) for res
            in burst_residuals]

    # Reconstructed SNR
    fftnorm = (1./opts.srate) * np.sqrt(len(bw_intervals[0][1])/2)
    burst_snrs = np.array([pycbc.filter.sigma(bw_intervals[i][1],
        low_frequency_cutoff=opts.flow)/fftnorm for i in xrange(len(ifos))])
    burst_net_snr = np.sqrt(sum(np.square(burst_snrs)))
    print 'burst_net_snr', burst_net_snr
    print 'busrt_snrs', burst_snrs


# --------------------------------------------------------------------------------
#
# CWB reconstruction
#
if opts.cwb_waveforms is not None:
    cwb_waves = []
    print "loading cwb"
    for cwbfile in opts.cwb_waveforms:
        cwb_data = np.loadtxt(cwbfile, skiprows=1)
        cwb_times = cwb_data[:,0]

        # Reduce to common segment length
        idx = (cwb_times>=opts.bw_epoch) * (cwb_times<opts.bw_epoch+opts.duration) 
        cwb_ML = cwb_data[idx,1]
        cwb_median = cwb_data[idx,2]
        cwb_epoch = cwb_times[idx][0] 
        if opts.cwb_injections: cwb_epoch += 1./opts.srate #bug

        cwb_waves.append(pycbc.types.TimeSeries(cwb_ML, epoch=cwb_epoch,
            delta_t=1./opts.srate))

    # Data - Reconstruction Residuals
    pe_burst_residuals = [wd-cw.data for wd, cw in zip(white_data, cwb_waves)]


    fftnorm = (1./opts.srate) * np.sqrt(len(cwb_waves[0])/2)
    burst_snrs = np.array([pycbc.filter.sigma(cwb_wave,
        low_frequency_cutoff=opts.flow)/fftnorm for cwb_wave in cwb_waves])
    burst_net_snr = np.sqrt(sum(np.square(burst_snrs)))



# --------------------------------------------------------------------------------
#
# LALInference (li)
#

if not opts.injections and not opts.cwb_injections:
    
    print "loading lalinference results"

    li_waves = np.load(opts.li_waveforms)

    if opts.nwaves is not None:
        nwaves = opts.nwaves
        idx = np.random.permutation(len(li_waves[0]))
        li_waves = [l[idx,:][:opts.nwaves,:] for l in li_waves]
    else:
        nwaves=np.shape(li_waves)[1]

    #summary_files = opts.li_summary_waveforms.split(',')
    summary_files = opts.li_summary_waveforms
    f = open(summary_files[0],'r')
    header = f.readline().replace('#','').split()
    f.close()

    amp_ratios = [np.loadtxt(summary_file,
        usecols=[header.index('amp_ratio')])[0] for summary_file in
        summary_files]

    li_best_hts_white = [pycbc.types.TimeSeries(np.loadtxt(summary_file,
        usecols=[header.index('whitened_ML')]), delta_t=1./opts.srate,
        epoch=opts.li_epoch) for summary_file in summary_files]

    fftnorm = (1./opts.srate) * np.sqrt(len(li_best_hts_white[0])/2)
    
    li_snrs = np.array([pycbc.filter.sigma(li_wave,
        low_frequency_cutoff=opts.flow)/fftnorm for li_wave in li_best_hts_white])
    li_net_snr = np.sqrt(sum(np.square(li_snrs)))
    print 'li_snrs ', li_snrs
    print 'li_net_snr ', li_net_snr

    # Get credible intervals for reconstructed T-domain waveforms
    li_intervals=[]
    for i in xrange(len(ifos)):
        li_intervals.append([pycbc.types.TimeSeries(ldata, delta_t=1./opts.srate,
            epoch=opts.li_epoch) for ldata in np.percentile(li_waves[i], [5, 50, 95],
                axis=0)])

    # Data - Reconstruction Residuals
    pe_li_residuals = [wd-li_interval[1].data for wd, li_interval in
            zip(white_data, li_intervals)]
    li_residuals = [wd - li for wd, li in zip(white_data, li_waves)]

    li_residuals_intervals = [np.percentile(res, [5, 50, 95], axis=0) for res
            in li_residuals]


    #
    # Figures of Merit
    #

    print "computing overlaps of bayeswave with lalinference"

    # --- Overlaps

    if opts.cwb_waveforms is not None:
        # Point-estimate overlaps: (MAP BBH | ML cWB)
        pe_overlaps = [pycbc.filter.match(cwb_waves[i], li_best_hts_white[i],
            low_frequency_cutoff=opts.flow)[0] for i in xrange(len(ifos))]

        pe_net_overlap = network_overlap(cwb_waves, li_best_hts_white,
                f_low=opts.flow)

        # Overlap distribution from burst point-estimate and LI
        multi_overlaps = np.zeros(shape=(len(ifos), nwaves))
        net_multi_overlaps = np.zeros(nwaves)

        for w in xrange(nwaves):
            li_ts = [pycbc.types.TimeSeries(li_waves[i,w], delta_t=1./opts.srate,
                epoch=opts.li_epoch) for i in xrange(len(ifos))]

            multi_overlaps[:,w] = [pycbc.filter.match(cwb_waves[i], li_ts[i],
                low_frequency_cutoff=opts.flow)[0] for i in xrange(len(ifos))]
            net_multi_overlaps[w] = network_overlap(cwb_waves, li_ts, f_low=opts.flow)

    else:
        # Point-estimate overlaps: (MAP BBH | median BWB)
        pe_overlaps = [pycbc.filter.match(bw_intervals[i][1],
            li_best_hts_white[i], low_frequency_cutoff=opts.flow)[0] for i in
            xrange(len(ifos))]

        bw_medians = [bw_intervals[i][1] for i in xrange(len(ifos))]

        pe_net_overlap = float(network_overlap(bw_medians, li_best_hts_white,
                f_low=opts.flow))


        # Overlap distribution from rnadom waveform draws
        overlaps = np.zeros(shape=(len(ifos), nwaves))
        net_overlaps = np.zeros(nwaves)

        # Overlap distribution from burst point-estimate and LI
        multi_overlaps = np.zeros(shape=(len(ifos), nwaves))
        net_multi_overlaps = np.zeros(nwaves)

        # Overlap of BW samples with LI best
        bw_li_best_overlaps = np.zeros(shape=(len(ifos), nwaves))
        bw_li_best_net_overlaps = np.zeros(nwaves)
        

        for w in xrange(nwaves):

            bw_ts = [pycbc.types.TimeSeries(bw_waves[i][w], delta_t=1./opts.srate,
                epoch=opts.bw_epoch) for i in xrange(len(ifos))]

            li_ts = [pycbc.types.TimeSeries(li_waves[i][w], delta_t=1./opts.srate,
                epoch=opts.li_epoch) for i in xrange(len(ifos))]

            overlaps[:,w] = [pycbc.filter.match(bw_ts[i], li_ts[i],
                low_frequency_cutoff=opts.flow)[0] for i in xrange(len(ifos))]
            net_overlaps[w] = network_overlap(bw_ts, li_ts, f_low=opts.flow)

            multi_overlaps[:,w] = [pycbc.filter.match(bw_medians[i], li_ts[i],
                low_frequency_cutoff=opts.flow)[0] for i in xrange(len(ifos))]
            net_multi_overlaps[w] = network_overlap(bw_medians, li_ts, f_low=opts.flow)

            bw_li_best_overlaps[:,w] = [pycbc.filter.match(bw_ts[i], li_best_hts_white[i],
                low_frequency_cutoff=opts.flow)[0] for i in xrange(len(ifos))]
            bw_li_best_net_overlaps[w] = network_overlap(bw_ts, li_best_hts_white, f_low=opts.flow)


    # --- Residuals
    
    # Point estimates: BBH - wavelets
    if opts.cwb_waveforms is not None:
        pe_residuals = [li_best.data-cwave.data for li_best, cwave in
                zip(li_best_hts_white, cwb_waves)]
    else:
        pe_residuals = [li_best.data-bw_interval[1].data for
                li_best, bw_interval in zip(li_best_hts_white, bw_intervals)]

        # Residual distribution from waveform draws
        residuals = np.array(li_waves) - np.array(bw_waves)



# --------------------------------------------------------------------------------
#
# Injections
#

def parse_snr(filepath):
    f=open(filepath,'r')
    snr={}
    for lines in f.readlines():
        parts=lines.split(':')
        snr[parts[0]] = float(parts[1])
    return snr

if opts.injections or opts.cwb_injections:

    print "Loading injected waveforms and computing overlaps"
    if opts.cwb_injections:
        injection_files = [glob.glob(os.path.join(opts.bw_dir,
            "{ifo}-pycbchwinj*".format(ifo=ifo)))[0] for ifo in ifos]
        epochs = [os.path.basename(infile).split('-')[2].split('.')[0] for
                infile in injection_files]
        inj_hts_white = [ pycbc.types.TimeSeries(np.loadtxt(infile),
            epoch=epoch, delta_t=1./opts.srate) for infile,epoch in
            zip(injection_files,epochs) ]
    else:
        inj_infile_fmt = os.path.join(opts.bw_dir,
                'post/injected_whitened_waveform.dat.{}')
        injection_files = [inj_infile_fmt.format(ifo) for ifo in
                xrange(len(ifos))]
        inj_hts_white = [ pycbc.types.TimeSeries(np.loadtxt(infile),
            epoch=opts.li_epoch, delta_t=1./opts.srate) for infile in
            injection_files ]

    snr = parse_snr(os.path.join(opts.bw_dir,'snr.txt'))


    # --- Overlaps

    # Point-estimate overlaps (Injected waveform|median wavelet)
    if opts.cwb_waveforms is not None:

        pe_overlaps = [pycbc.filter.overlap(cwb_waves[i],
            inj_hts_white[i], low_frequency_cutoff=opts.flow) for i in
            xrange(len(ifos))]

        pe_net_overlap = network_overlap(cwb_waves, inj_hts_white,
                f_low=opts.flow)

    else:
        pe_overlaps = [pycbc.filter.overlap(bw_intervals[i][1],
            inj_hts_white[i], low_frequency_cutoff=opts.flow) for i in
            xrange(len(ifos))]

        bw_medians = [bw_intervals[i][1] for i in xrange(len(ifos))]
        pe_net_overlap = network_overlap(bw_medians, inj_hts_white,
                f_low=opts.flow)

        # Overlap distribution from waveform draws
        overlaps = np.zeros(shape=(len(ifos), nwaves))
        net_overlaps = np.zeros(nwaves)
        for w in xrange(nwaves):

            bw_ts = [pycbc.types.TimeSeries(bw_waves[i][w], delta_t=1./opts.srate,
                epoch=opts.bw_epoch) for i in xrange(len(ifos))]

            overlaps[:,w] = [pycbc.filter.overlap(bw_ts[i], inj_hts_white[i],
                low_frequency_cutoff=opts.flow) for i in xrange(len(ifos))]

            net_overlaps[w] = network_overlap(bw_ts, inj_hts_white, f_low=opts.flow)


    # --- Residuals
    
    # Point estimates: BBH - wavelets
    if opts.cwb_waveforms is not None:
        pe_residuals = [ inj.data-cwave.data for inj, cwave in
                zip(inj_hts_white,cwb_waves) ]
    else:
        pe_residuals = [ inj.data-bw_interval[1].data for inj,bw_interval in
                zip(inj_hts_white,bw_intervals) ]

        # Residual distribution from waveform draws
        residuals = np.array([inj - bw for inj, bw in zip(inj_hts_white, bw_waves)])




# -------------------------------------------------------------------------------
#
# Save summary statistics
#


# --- Intervals
if opts.cwb_waveforms is None:

    overlap_intervals = [np.percentile(overlap, [5, 50, 95]) for overlap in
        overlaps]
    net_overlap_intervals = np.percentile(net_overlaps, [5, 50, 95])

    residuals_intervals = [np.percentile(res, [5, 50, 95], axis=0) for res
            in residuals]

    local_times = bw_intervals[0][1].sample_times-opts.trigtime

else:

    local_times = cwb_waves[0].sample_times-opts.trigtime

multi_overlap_intervals = [np.percentile(multi_overlap, [5, 50, 95]) for
        multi_overlap in multi_overlaps]
net_multi_overlap_intervals = np.percentile(net_multi_overlaps, [5, 50, 95])

print 'Saving stats'

header = "# pe_snr burst_snr point_estimate_overlap\n"
for i in xrange(len(ifos)):

    fname = opts.output_dir +'/'+ ifos[i]+'_stats.txt'

    save_arr = np.array([li_snrs[i],  burst_snrs[i], pe_overlaps[i]])
    f=open(fname,'w')
    result = [f.writelines('%f\t'%val) for val in save_arr.tolist()]
    f.writelines('\n')
    f.close()

# Network results
header = "# pe_net_snr burst_net_snr point_estimate_net_overlap\n"

fname = opts.output_dir+'/NET'+'_stats.txt'


save_arr = np.array([li_net_snr, burst_net_snr, pe_net_overlap])

f=open(fname,'w')
#f.writelines(header)
result = [f.writelines('%f\t'%val) for val in save_arr.tolist()]
f.writelines('\n')
f.close()

# -------------------------------------------------------------------------------



if opts.save_for_publication:
    print "pickling plot data"

    import cPickle as pickle

    # Convert ifo-lists of intervals lists to pickle-able format
    save_dict = {'ifos':ifos, 'opts':opts,
            'local_times':local_times,
            'white_data':white_data,
            'pe_residuals':pe_residuals,
            'pe_burst_residuals':pe_burst_residuals,
            'pe_overlaps':pe_overlaps,
            'pe_net_overlap':pe_net_overlap,
            'burst_snrs':burst_snrs,
            'burst_net_snr':burst_net_snr,
            'li_snrs':li_snrs,
            'li_net_snr':li_net_snr,
            'amp_ratio':[a for a in amp_ratios]
            }

    save_dict['multi_overlap_intervals']=multi_overlap_intervals
    save_dict['net_multi_overlap_intervals']=net_multi_overlap_intervals
    save_dict['multi_overlaps']=multi_overlaps
    save_dict['net_multi_overlaps']=net_multi_overlaps

    if opts.cwb_waveforms is not None:
        save_dict['cwb_waves']=[cwave.data for cwave in cwb_waves]
    else:
        save_dict['bw_intervals']=[ [ts.data for ts in bw ] for bw in bw_intervals ]
        save_dict['residuals_intervals']=residuals_intervals
        save_dict['burst_residuals_intervals']=burst_residuals_intervals
        save_dict['overlap_intervals']=overlap_intervals
        save_dict['net_overlap_intervals']=net_overlap_intervals
        save_dict['overlaps']=overlaps
        save_dict['net_overlaps']=net_overlaps
        save_dict['bw_li_best_overlaps'] = bw_li_best_overlaps
        save_dict['bw_li_best_net_overlaps'] = bw_li_best_net_overlaps
#        save_dict['bw_waves']=bw_waves


    if opts.injections:
        save_dict['inj_hts_white']=[inj.data for inj in inj_hts_white]
        save_dict['snr'] = snr
    else: 
        save_dict['li_intervals'] = [ [li.data for li in li_interval] for
                li_interval in li_intervals ]
        save_dict['li_best_hts_white'] = [li_best.data for li_best in
                li_best_hts_white]
        save_dict['li_residuals_intervals'] = li_residuals_intervals
#        save_dict['li_waves']=li_waves

    fname = opts.output_dir+'/plotdata.pickle'
    pickle.dump(save_dict, open(fname, "wb"))

