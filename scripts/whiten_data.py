#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Copyright (C) 2016-2017 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import sys, string
import numpy as np
from optparse import OptionParser
import lal
import pycbc.types
import pycbc.filter

def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = OptionParser()
    parser.add_option("-i", "--input-data", type=str)
    parser.add_option("-t", "--trigger-time", type=float, default=1167559936.6)
    parser.add_option("-d", "--duration", type=int, default=4.0)
    parser.add_option("-a", "--spectrum", type=str)
    parser.add_option("-s", "--sample-rate", type=int, default=16384)
    parser.add_option("-p", "--padding", type=float, default=0.4)

    (opts,args) = parser.parse_args()

    name_parts = opts.input_data.split('-')
    name_parts[2] = str(int(opts.trigger_time-0.5*opts.duration))
    name_parts[3] = str(int(opts.duration))
    opts.output_name = string.join(name_parts,'-')

    return opts, args

# Parse/load input:
opts, args = parser()
strain_data = np.loadtxt(opts.input_data)
asd_data = np.loadtxt(opts.spectrum)

# Extract and window strain series
times=strain_data[:,0]
trigidx=np.argmin(abs(opts.trigger_time-times))
seglen=int(opts.duration*opts.sample_rate)
idx=range(trigidx-int(0.5*seglen), trigidx+int(0.5*seglen))

hofT = pycbc.types.TimeSeries(strain_data[idx,1],
        epoch=times[idx][0], delta_t=1./opts.sample_rate)

new_srate = 2*max(asd_data[:,0])
hofT = pycbc.filter.resample_to_delta_t(hofT, 1./new_srate)

window = lal.CreateTukeyREAL8Window(len(hofT),
    2.0*opts.padding*new_srate/len(hofT))
hofT.data *= window.data.data

# FFT & whiten
HofF = hofT.to_frequencyseries()
SofF = np.interp(HofF.sample_frequencies.data, asd_data[:,0], asd_data[:,1])

HofF_white = pycbc.types.FrequencySeries(HofF.data/SofF, delta_f=HofF.delta_f,
        epoch=hofT.start_time)


# Save
HofF_white.save(opts.output_name + '_whitenedCPLXspectrum.txt')

hofT_white = HofF_white.to_timeseries()
#hofT_white = pycbc.filter.highpass(hofT_white, min(asd_data[:,0]), filter_order=12)

htmp = lal.CreateREAL8TimeSeries('H1', hofT_white.start_time, 0.0, 1./2048,
        lal.StrainUnit, len(hofT_white))
htmp.data.data = np.copy(hofT.data)

lal.HighPassREAL8TimeSeries(htmp, 35.0, 1-1e-3, 8)
lal.LowPassREAL8TimeSeries(htmp, 350.0, 1-1e-3, 8)
#lal.HighPassREAL8TimeSeries(htmp, min(asd_data[:,0]), 1-1e-3, 8)

hofT_white_high = HofF_white.to_timeseries()
hofT_white_high.data = np.copy(htmp.data.data)

HofF_white_high = hofT_white_high.to_frequencyseries()

from matplotlib import pyplot as plt
f, ax = plt.subplots()
ax.plot(hofT_white.sample_times - opts.trigger_time, hofT_white_high)
ax.set_xlim(-0.1, 0.02)
plt.show()



