#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Produce publication-quality comparison plots of NR data

Requires pickled data NR-summary.pickle
"""

import sys, os
import numpy as np
import cPickle as pickle

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt

import argparse

import filt


def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--pickled-nr-results", metavar="NR_PICKLE",
            type=str, default=None, help="Pickled NR data from\
                    gwcomp_nrmatch.py",
                    required=True)
    parser.add_argument("--output-filename", default="GW170104", type=str,
            help="Identifier to attach to output files")
    parser.add_argument("--xlims", default=[0.425, 0.625],
            help="x-axis limits for main panel", type=two_floats)
    parser.add_argument("--ylims", default=[-4, 4],
            help="y-axis limits for main panel", type=two_floats)
    parser.add_argument("--raw-ylims", default=[-1e-21, 1e-21],
            help="y-axis limits for main panel", type=two_floats)

    parser.add_argument("--reconstruction-residuals", default=False,
            action="store_true", help="""
            boolean to produce insets showing the
            residuals between reconstruction methods""")

    parser.add_argument("--passband", default=(35.0, 350.0), nargs=2,
            type=float, 
            help="band-pass observed data at this frequency for plotting")
    parser.add_argument("--filter-order", default=8, type=int,
            help="low-pass filter order")

    parser.add_argument("--Lshift", default=2.93e-3, type=float,
            help="s by which to shift L1 back in combined data plots")

    args = parser.parse_args()

    return opts

def two_floats(value):
    values = value.split()
    if len(values) != 2:
        raise argparse.ArgumentError
    values = map(float, values)
    return values


fig_width_pt = 246.0  # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0/72.27               # Convert pt to inches
golden_mean = (np.sqrt(5)-1.0)/2.0         # Aesthetic ratio
fig_width = fig_width_pt*inches_per_pt  # width in inches
fig_height =fig_width*golden_mean       # height in inches
fig_size = [fig_width,fig_height]
params = {
          'axes.labelsize': 8,
          'font.size': 8,
          'legend.fontsize': 8,
          'xtick.color': 'k',
          'xtick.labelsize': 8,
          'ytick.color': 'k',
          'ytick.labelsize': 8,
          'text.usetex': True,
          'text.color': 'k',
          'figure.figsize': fig_size
          }
import pylab
pylab.rcParams.update(params)


