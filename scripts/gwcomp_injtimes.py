#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2018-2019 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Generate a list of times to place injections in off-source data around a GW."""
import argparse
import numpy as np

def parse_cmdline():
    """Parse command line"""

    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("--gps-start", type=float, required=True,
                        help="""Start time for injections""")
    parser.add_argument("--gps-end", type=float, required=True,
                        help="""End time for injections""")
    parser.add_argument("--gps-trigger", type=float, default=None,
                        help="""Trigger GPS time to avoid""")
    parser.add_argument("--pad-trigger", type=float, default=2.0,
                        help="""No injections will be placed in this padding
                        around trigger""")
    parser.add_argument("--gps-spacing", type=float, default=5.0,
                        help="""Mean spacing between injection times""")
    parser.add_argument("--N", type=int, default=None,
                        help="""Random jitter added to each injection time""")
    parser.add_argument("--jitter", type=float, default=1.0,
                        help="""Random jitter added to each injection time""")
    parser.add_argument("--output-filename", type=str, default="""injection_times.txt""",
                        help="Write injection times to this file")
    cmdline_args = parser.parse_args()

    return cmdline_args

## Input
ARGS = parse_cmdline()

## Generate Times
if ARGS.N is not None:
    TIMES = np.linspace(ARGS.gps_start, ARGS.gps_end, ARGS.N)
    SPACING = (ARGS.gps_end  - ARGS.gps_start)/ARGS.N
    print "Generating {0} injections in GPS [{1},{2}] (average spacing: {3:.2f}s)".format(
        ARGS.N, ARGS.gps_start, ARGS.gps_end, SPACING)
else:
    TIMES = np.arange(ARGS.gps_start, ARGS.gps_end + ARGS.gps_spacing, ARGS.gps_spacing)
    print "Generating {0} injections in GPS [{1},{2}] (average spacing: {3:.2f}s)".format(
        len(TIMES), ARGS.gps_start, ARGS.gps_end, ARGS.gps_spacing)

## Add jitter
OFFSETS = 0.5*ARGS.jitter + ARGS.jitter*np.random.rand(len(TIMES))
TIMES += OFFSETS

## Excise times too close to trigger
if ARGS.gps_trigger is not None and (abs(TIMES-ARGS.gps_trigger) >
                                     ARGS.pad_trigger).any():

    print "Excising {} times (too close to GW)".format(
        sum(abs(TIMES-ARGS.gps_trigger) > ARGS.pad_trigger))

    TIMES = np.delete(TIMES,
                      np.where(abs(TIMES-ARGS.gps_trigger) > ARGS.pad_trigger))

## Write output
F = open(ARGS.output_filename, 'w')
F.writelines("# Trigger times for BayesWave\n")
for TIME in TIMES:
    F.writelines("{:f}\n".format(TIME))
F.close()
print "Times written to {}".format(ARGS.output_filename)
