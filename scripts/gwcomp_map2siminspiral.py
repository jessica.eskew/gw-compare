#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       gwcomp_map2siminspiral.py
#
#       Copyright 2018
#       Benjamin Farr <bfarr@u.northwestern.edu>,
#       Will M. Farr <will.farr@ligo.org>,
#       John Veitch <john.veitch@ligo.org>
#       James Clark <james.clark@ligo.org>
#
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
"""
Populate a sim_inspiral table with MAP params and user-specified times from a
posterior_samples.data file.  Modification of cbcBayesPosToSimInspiral.py which
originally populates the sim_inspiral table with random draws from the
posterior.
"""
import os,sys
from optparse import Option, OptionParser
import numpy as np
from glue.ligolw import ligolw
from glue.ligolw import lsctables
from glue.ligolw import ilwd
import matplotlib
matplotlib.use("Agg") # Needed to run on the CIT cluster
from pylal import bayespputils as bppu

from pycbc.inject import InjectionSet, legacy_approximant_name
from pycbc.types import TimeSeries, FrequencySeries
from pycbc.waveform import get_td_waveform
from pycbc.filter import sigma as pycbc_snr

# Create a datatype for all relavent fields to be filled in the sim_inspiral table
sim_inspiral_dt = [
        ('waveform','|S64'),
        ('taper','|S64'),
        ('f_lower', 'f8'),
        ('mchirp', 'f8'),
        ('eta', 'f8'),
        ('mass1', 'f8'),
        ('mass2', 'f8'),
        ('geocent_end_time', 'f8'),
        ('geocent_end_time_ns', 'f8'),
        ('distance', 'f8'),
        ('longitude', 'f8'),
        ('latitude', 'f8'),
        ('inclination', 'f8'),
        ('coa_phase', 'f8'),
        ('polarization', 'f8'),
        ('spin1x', 'f8'),
        ('spin1y', 'f8'),
        ('spin1z', 'f8'),
        ('spin2x', 'f8'),
        ('spin2y', 'f8'),
        ('spin2z', 'f8'),
        ('amp_order', 'i4'),
        ('numrel_data','|S64')
]

def get_input_filename(parser, args):
    """Determine name of input: either the sole positional command line argument,
    or /dev/stdin."""
    if len(args) == 0:
        infilename = '/dev/stdin'
    elif len(args) == 1:
        infilename = args[0]
    else:
        parser.error("Too many command line arguments.")
    return infilename

def update_progress(progress):
    print '\r\r[{0}] {1}%\n'.format('#'*(progress/2)+' '*(50-progress/2), progress),
    sys.stdout.flush()

def standardize_param_name(params, possible_names, desired_name):
    for name in possible_names:
        if name in params: params[params.index(name)] = desired_name

def standardize_param_names(params):
    standardize_param_name(params, ['m1'], 'm1')
    standardize_param_name(params, ['m2'], 'm2')
    standardize_param_name(params, ['mc', 'chirpmass'], 'mchirp')
    standardize_param_name(params, ['massratio'], 'eta')
    standardize_param_name(params, ['d', 'dist'], 'distance')
    standardize_param_name(params, ['ra'], 'longitude')
    standardize_param_name(params, ['dec'], 'latitude')
    standardize_param_name(params, ['iota'], 'inclination')
    standardize_param_name(params, ['phi', 'phase', 'phi0'], 'phi_orb')
    standardize_param_name(params, ['psi', 'polarisation'], 'polarization')


def compute_mass_parameterizations(samples):
    params = samples.keys()
    has_mc = 'mchirp' in params
    has_eta = 'eta' in params
    has_q = 'q' in params
    has_ms = 'mass1' in params and 'mass2' in params
    has_mtotal = 'mtotal' in params

    if has_mc:
        mc = samples['mchirp']
        if not has_eta:
            if has_q:
                eta = bppu.q2eta(mc, samples['q'])
            else:
                raise ValueError("Chirp mass given with no mass ratio.")
        else:
            eta = samples['eta']

        if not has_ms:
            m1, m2 = bppu.mc2ms(mc, eta)

        mtotal = m1 + m2

    elif has_ms:
        m1 = samples['mass1']
        m2 = samples['mass2']
        mtotal = m1 + m2
        eta = m1 * m2 / (mtotal * mtotal)
        mc = mtotal * np.power(eta, 3./5.)

    elif has_mtotal:
        mtotal = samples['mtotal']
        if has_eta:
            eta = samples['eta']
            mc = mtotal * np.power(eta, 3./5.)
            m1, m2 = bppu.mc2ms(mc, eta)
        elif has_q:
            m1 = mtotal / (1 + samples['q'])
            m2 = mtotal - m1
        else:
            raise ValueError("Chirp mass given with no mass ratio.")
    return mc, eta, m1, m2, mtotal

def generate_strains(injection_file, sim, ifos=['H1','L1'], delta_t=1./2048, fmin=8.):
    """
    Generate strain TimeSeries for each IFO in IFOs for sim_id in the
    injection_file
    """

    injections = InjectionSet(injection_file)

    # parse the sim_inspiral waveform column
    name, phase_order = legacy_approximant_name(sim.waveform)

    h_plus, _ = get_td_waveform(sim, approximant=name, phase_order=phase_order,
            delta_t=delta_t)

    datalen=4
    start_time = sim.geocent_end_time-0.5*datalen
    num_samples = int(datalen/delta_t)

    # loop over IFOs for writing waveforms to file
    output=[]
    for i,ifo in enumerate(ifos):

        # create a time series of zeroes to inject waveform into
        initial_array = np.zeros(num_samples, dtype=h_plus.dtype)
        output.append(TimeSeries(initial_array, delta_t=delta_t,
                epoch=start_time, dtype=h_plus.dtype))

        # inject waveform into time series of zeroes
        injections.apply(output[i], ifo, simulation_ids=[sim.simulation_id])

    return output


def compute_snr(strains, psd_path, ifos=['H1','L1'], fmin=20.0):
    """
    Compute network SNR for H,L using BayesWave PSDs in psd_path
    """

    # Load PSD data
    bw_psd_data = [np.loadtxt(os.path.join(psd_path, "IFO{}_psd.dat".format(i)))
            for i in xrange(len(ifos))]

    netsnr=0.0
    for strain, bw_psd in zip(strains,bw_psd_data):

        # Interpolate PSD to strain frequencies
        Strain = strain.to_frequencyseries()
        sample_frequencies = Strain.sample_frequencies.data

        psd_interp = 10**np.interp(sample_frequencies, bw_psd[:,0],
                np.log10(bw_psd[:,1]))

        psd = FrequencySeries(psd_interp, delta_f=Strain.delta_f)

        # Compute SNR
        netsnr += pycbc_snr(strain, psd, low_frequency_cutoff=fmin)**2

    return np.sqrt(netsnr)


if __name__ == "__main__":
    parser = OptionParser(
            description = __doc__,
            usage = "%prog [options] [INPUT]",
            option_list = [
                Option("-o", "--output", metavar="FILE.xml",
                    help="name of output XML file"),
                Option("--approx", metavar="APPROX", default="IMRPhenomPv2pseudoFourPN",
                    help="approximant to be injected"),
                Option("--taper", metavar="TAPER", default="TAPER_NONE",
                    help="Taper methods for injections"),
                Option("--amporder", metavar="AMPORDER", type=int, default=0,
                    help="pN order in amplitude for injection"),
                Option("--gps-start", metavar="GPSSTART", type=float,
                    default=None, help="time to start injections"),
                Option("--time-step", metavar="TIMESTEP", type=float,
                    default=5.0, help="Seconds between injections"),
                Option("--gps-end", metavar="GPSEND", type=float, default=None,
                    help="time to stop injections"),
                Option("--event-padding", metavar="EVENTPAD", type=float,
                    default=5.0, help="""Excise injections from a window
                    this wide around the trigger"""),
                Option("--fix-snr", metavar="FIXSNR", default=False,
                    action="store_true", help="Fix the network SNR"),
                Option("--psd-path", metavar="PSDS", type=str,
                    default=None, help="""Directory with BayesWave PSDs for
                    injection times.  Code expects BayesWave PSDs in
                    GPS-labelled directories"""),
                Option("--map-snr", metavar="MAPSNR", type=float,
                    default=12.6760132186, help="""Optimal network SNR to scale
                    injections to"""),
                Option("--ifos", metavar="IFOS", type=str, default="H1,L1",
                    help="Comma-separated list of instruments")
            ]
    )

    opts, args = parser.parse_args()
    infilename = args[0]
    ifos = opts.ifos.split(',')

    #
    # Load and parse posterior samples file
    #
    print "Parsing posterior samples file"
    peparser = bppu.PEOutputParser('common')
    resultsObj = peparser.parse(open(infilename, 'r'))
    posterior = bppu.Posterior(resultsObj)

    #
    # Get MAP sample to waveform params
    #
    samples = posterior.maxP[1]
    params = samples.keys()


    # Generate list of injection times and excise anything too close to the
    # maxP time
    times = np.arange(opts.gps_start, opts.gps_end, opts.time_step)
    times = times[abs(times-samples['time'])>opts.event_padding]
    N = len(times)

    # Create an empty structured array with names indentical to sim_inspiral fields
    injections = np.zeros((N,), dtype=sim_inspiral_dt)

    # Determine all mass parameterizations
    mc, eta, m1, m2, mtotal = compute_mass_parameterizations(samples)

    # Get cycle numbers as simulation_ids
    ids = range(N)

    # Compute cartesian spins
    if 'a1' in params and 'theta1' in params and 'phi1' in params:
        s1x, s1y, s1z = bppu.sph2cart(samples['a1'], samples['theta1'], samples['phi1'])
    elif 'a1z' in params:
        s1z = samples['a1z']
        s1x = np.zeros_like(s1z)
        s1y = np.zeros_like(s1z)
    else:
        s1x = np.zeros_like(m1)
        s1y = np.zeros_like(m1)
        s1z = np.zeros_like(m1)


    if 'a2' in params and 'theta2' in params and 'phi2' in params:
        s2x, s2y, s2z = bppu.sph2cart(samples['a2'], samples['theta2'], samples['phi2'])
    elif 'a2z' in params:
        s2z = samples['a2z']
        s2x = np.zeros_like(s2z)
        s2y = np.zeros_like(s2z)
    else:
        s2x = np.zeros_like(m2)
        s2y = np.zeros_like(m2)
        s2z = np.zeros_like(m2)

    system_frame_params = set([ \
            'costheta_jn', \
            'phi_jl', \
            'tilt1', 'tilt2', \
            'phi12', \
            'a1','a2', \
            'f_ref' \
    ])
    theta_jn=np.arccos(samples['costheta_jn'])
    if set(params).intersection(system_frame_params) == system_frame_params:
        inclination, theta1, phi1, theta2, phi2, _ = bppu.physical2radiationFrame(
                theta_jn,
                samples['phi_jl'],
                samples['tilt1'],
                samples['tilt2'],
                samples['phi12'],
                samples['a1'],
                samples['a2'],
                m1, m2,
                samples['f_ref'])
        inclination = inclination.flatten()
        theta1 = theta1.flatten()
        phi1 = phi1.flatten()
        theta2 = theta2.flatten()
        phi2 = phi2.flatten()
        s1x, s1y, s1z = bppu.sph2cart(samples['a1'], theta1, phi1)
        s2x, s2y, s2z = bppu.sph2cart(samples['a2'], theta2, phi2)
    else:
        inclination = theta_jn

    flow = samples['flow']

    # Populate structured array
    injections['waveform'] = [opts.approx for i in xrange(N)]
    injections['taper'] = [opts.taper for i in xrange(N)]
    injections['f_lower'] = flow*np.ones(N)
    injections['mchirp'] = mc*np.ones(N)
    injections['eta'] = eta*np.ones(N)
    injections['mass1'] = m1*np.ones(N)
    injections['mass2'] = m2*np.ones(N)
    injections['geocent_end_time'] = np.modf(times)[1]
    injections['geocent_end_time_ns'] = np.modf(times)[0] * 10**9
    injections['distance'] = samples['distance']*np.ones(N)
    injections['longitude'] = samples['ra']*np.ones(N)
    injections['latitude'] = samples['dec']*np.ones(N)
    injections['inclination'] = inclination*np.ones(N)
    injections['coa_phase'] = samples['phase']*np.ones(N)
    injections['polarization'] = samples['psi']*np.ones(N)
    injections['spin1x'] = s1x*np.ones(N)
    injections['spin1y'] = s1y*np.ones(N)
    injections['spin1z'] = s1z*np.ones(N)
    injections['spin2x'] = s2x*np.ones(N)
    injections['spin2y'] = s2y*np.ones(N)
    injections['spin2z'] = s2z*np.ones(N)
    injections['amp_order'] = [opts.amporder for i in xrange(N)]
    injections['numrel_data'] = [ "" for _ in xrange(N)]

    # Create a new XML document
    xmldoc = ligolw.Document()
    xmldoc.appendChild(ligolw.LIGO_LW())
    sim_table = lsctables.New(lsctables.SimInspiralTable)
    xmldoc.childNodes[0].appendChild(sim_table)

    # Add empty rows to the sim_inspiral table
    for inj in xrange(N):
        row = sim_table.RowType()
        for slot in row.__slots__: setattr(row, slot, 0)
        sim_table.append(row)

    # Fill in IDs
    for i,row in enumerate(sim_table):
        row.process_id = ilwd.ilwdchar("process:process_id:{0:d}".format(i))
        row.simulation_id = ilwd.ilwdchar("sim_inspiral:simulation_id:{0:d}".format(ids[i]))

    # Fill rows
    for field in injections.dtype.names:
        vals = injections[field]
        for row, val in zip(sim_table, vals): setattr(row, field, val)

    # Write file
    output_file = open(opts.output, 'w')
    xmldoc.write(output_file)
    output_file.close()

    # Scale to fixed SNR (new file)
    print "Locating PSDs"
    if opts.fix_snr:
        psd_dirs=[os.path.join(opts.psd_path,name) for name in
                os.listdir(opts.psd_path) if
                os.path.isdir(os.path.join(opts.psd_path, name))]
        psd_times = [float(psd_dir.split('/')[-1]) for psd_dir in psd_dirs]

        # Get paths for PSDs closest to each injection time
        psd_dirs = [psd_dirs[np.argmin(abs(time-psd_times))] for time in times]

        # Loop through injections and scale to map-snr 
        print "Generating waveforms and rescaling to SNR={}".format(
                opts.map_snr)

        for s, (sim, psd_dir) in enumerate(zip(sim_table,psd_dirs)):
            update_progress(100*(s+1)/len(sim_table))

            # Generate strains for this injection
            strains = generate_strains(opts.output, sim)

            # Compute network SNR
            snr = compute_snr(strains, psd_dir)
            print "original SNR: {}".format(snr)

            scale = snr/opts.map_snr
            setattr(sim, "distance", sim.distance*scale)
            print "Rescaling distance by {:.2f}".format(scale)
            for ifo in ifos:
                attrname='eff_dist_'+ifo[0].lower()
                effdist=getattr(sim,attrname)
                setattr(sim,attrname,effdist*scale)

        # Create a new XML document
        xmldoc = ligolw.Document()
        xmldoc.appendChild(ligolw.LIGO_LW())
        xmldoc.childNodes[0].appendChild(sim_table)

        # Write file
        output_file = open(opts.output.replace(".xml",
            "_SNR-{:.2f}.xml".format(opts.map_snr)), 'w')
        xmldoc.write(output_file)
        output_file.close()


