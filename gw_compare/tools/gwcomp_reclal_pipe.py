##################################################################################
# Copyright (C) 2020 Isaac Chun Fung Wong
# Copyright (C) 2020 Sudarshan Ghonge
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
##################################################################################

##
# @file gwcomp_reclal_pipe.py
# @author Sudarshan Ghonge
# @brief This program generates A DAG file for generating time domain reconstructions
# using reclal

from __future__ import print_function
import argparse
from string import Template

def parser():
	parser = argparse.ArgumentParser(description="This helper program parses info from config file and save to txt.")
	parser.add_argument("--get",type=str,help="Path to generate a config file.",required=False)
	parser.add_argument("--config",type=str,help="Path to a completed config file.",required=False)
	parser.add_argument("--submit",help="Submit the job",action="store_true")
	return parser

def main(args=None):
	import ast
	from ligo.gracedb.rest import GraceDb
	import os, stat
	import glob
	from pycondor import Dagman,Job
	import pycondor.utils
	from configparser import ConfigParser
	from termcolor import cprint
	import subprocess
	import shlex
	import json
	import numpy as np
	from shutil import copyfile

	opts = parser().parse_args(args)

	if opts.get is not None:
		config = ConfigParser(allow_no_value=True)
		config.optionxform = str
		config.add_section("condor")
		config.set("condor","accounting-group-user",os.popen("whoami").read().strip())
		config.set("condor","accounting-group","ligo.prod.o3.cbc.testgr.tiger")
		config.set("condor","universe","vanilla")
		config.add_section("reclal")
		config.set("reclal","request-memory","8GB")
		config.set("reclal","request-disk","8GB")
		config.set("reclal","# List of super-events to process")
                config.set("reclal","# Superevent IDs separated by space.")
		config.set("reclal","superevents","")

		with open(opts.get,"w") as f:
			config.write(f)
	elif opts.config is not None:
		config = ConfigParser()
		config.optionxform = str
		config.read(opts.config)

		ISSUES = {"missing-PEs":[]}

		error = os.path.abspath("condor_reclal/error")
		output = os.path.abspath("condor_reclal/output")
		log = os.path.abspath("condor_reclal/log")
		submit = os.path.abspath("condor_reclal/submit")
		extra_lines = [
				"accounting_group_user = %s"%config["condor"]["accounting-group-user"],
				"accounting_group = %s"%config["condor"]["accounting-group"],
				"stream_error = True",
				"stream_output = True"]
		dag_reclal = Dagman(name="reclal_pipe",submit=submit)
		superevents = config["reclal"]["superevents"].split()

		srate_max = 2048
		seglen_max = 8
		pe_dir = "/home/sudarshan.ghonge/Projects/O3_PE/o3a_catalog_events"
		pe_data_dir = "/home/sudarshan.ghonge/Projects/O3a/C01_runs"
		# Assumed directory to store config files of preferred runs.
		pwd = os.getcwd()
		user = os.popen("whoami").read().strip()
		for event in superevents:
			cprint("Processing superevent %s..."%event, "yellow")
			if not os.path.isdir(event):
				os.mkdir(event)
			# Locate the config files.
			pe_file = glob.glob(os.path.join(pe_data_dir, event, "PE", "*.dat"))
			print(pe_file)
			if len(pe_file)==0:
				cprint("ERROR: no PE file(s) found for %s. Please contact owner of the catalog directory."%event,"red")
				ISSUES["missing-PEs"].append(event)
				continue
			pe_file = pe_file[0]
			# 
			pe_config_file  = glob.glob(os.path.join(pe_dir, event, 'C01_offline', 'Prod0*.ini'))
			approx = 'IMRPhenomPv2' # This is prod0
                        config_pe = ConfigParser()
                        config_pe.optionxform = str
                        config_pe.read(pe_config_file)
                        ifos = ast.literal_eval(config_pe["analysis"]["ifos"])
			
			trigtime_file = glob.glob(os.path.join(pe_dir, event, "C01_offline", "*gps*.txt".format(event)))
                        trigtime = np.asscalar(np.loadtxt(trigtime_file[0]))

			event_dir = os.path.abspath(event)
			rundir = os.path.abspath(os.path.join(event,'Prod0', 'LI_reconstruct'))
			if not os.path.isdir(rundir):
				os.mkdir(rundir)
                        seglen = config_pe.getfloat("engine","seglen")
                        #if seglen > seglen_max:
                        #        cprint("WARNING: seglen (%s) in PE config is greater than the default maximum (%s). seglen is set to the default maximum value."%(seglen,seglen_max),"red")
                        #        seglen = seglen_max
                        srate = config_pe.getfloat("engine","srate")
                        #if srate>srate_max:
                        #        cprint("WARNING: srate (%s) in PE config is greater than the default maximum (%s). srate is set to the default maximum value."%(srate,srate_max),"red")
                        #        srate = srate_max
                        flows = ast.literal_eval(config_pe["lalinference"]["flow"])
                        flows = {ifo:float(flows[ifo]) for ifo in ifos}
                        flow = np.max(flows.values())
			try:
			    fref = config_pe.getfloat("analysis", "fref")
                        except:
		            fref = 20

			job_reclal_arg = "--li-samples-file %s --ifos %s --srate %s --duration %s --li-epoch %.9f --trigtime %.9f --output-dir %s --make-plots --calibrate --choose-fd --approx %s --fref %.3f --flow %.3f --nwaves 200 "%(pe_file," ".join(ifos),int(float(srate)),seglen,trigtime - seglen/2, trigtime,rundir,approx, fref, flow)
			job_reclal_arg += " --psd %s"%(" ".join([os.path.join(event_dir,"PSD","%s_PSD.dat"%(ifo)) for ifo in ifos]))
			job_reclal = Job(
					name="reclal_%s_%s"%(event,'Prod0'),
					executable="%s"%(os.popen("which gwcomp_reclal.py").read().strip()),
					error=error,
					output=output,
					log=log,
					submit=submit,
					request_memory=config["reclal"]["request-memory"],
					request_disk=config["reclal"]["request-disk"],
					getenv=True,
					universe=config["condor"]["universe"],
					extra_lines=extra_lines,
					dag=dag_reclal)
			job_reclal.add_arg(str(job_reclal_arg))
			#command = "gwcomp_reclals.py "+job_reclal_arg
			#out, err = reclal_proc.communicate()
		if opts.submit:
			dag_reclal.build_submit()
		else:
			dag_reclal.build()
		bw_run_f.close()
		os.chmod("bayeswave_run.sh",stat.S_IRWXU)
		# Print out all issues.
		if len(ISSUES.values())>0:
			cprint("##########","yellow")
			cprint("# ISSUES #","yellow")
			cprint("##########","yellow")
			cprint("The following superevent(s) is/are missing in %s:"%pe_data_dir,"yellow")
			if len(ISSUES["missing-PEs"])==0:
				cprint("---","yellow")
			else:
				for ele in ISSUES["missing-PEs"]:
					cprint(ele,"yellow")
