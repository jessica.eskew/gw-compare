##################################################################################
# Copyright (C) 2020 Isaac Chun Fung Wong
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
##################################################################################

##
# @file gwcomp_bw_onsource_pipe.py
# @author Isaac Chun Fung Wong
# @brief This program generate a bash scrip to perform BayesWave onsource run.

from __future__ import print_function
import argparse
from string import Template

bw_bash_template = """#!/bin/bash

source /home/sudarshan.ghonge/opt/bayeswave_master/etc/bayeswave-user-env.sh
bayeswave_pipe -r $workdir $bwflags $args $config --osg-deploy

condor_submit_dag $workdir/$rundir.dag
"""
bw_bash_template = Template(bw_bash_template)

def parser():
	parser = argparse.ArgumentParser(description="This program generates a bash script to perform BayesWave onsource run.")
	parser.add_argument("--get",type=str,help="Path to generate a config file.",required=False)
	parser.add_argument("--config",type=str,help="Path to a completed config file.",required=False)
	parser.add_argument("--submit",help="Submit the job",action="store_true")
	return parser

def main(args=None):
	from configparser import ConfigParser
	import os

	opts = parser().parse_args(args)

	if opts.get is not None:
		config = ConfigParser(allow_no_value=True)
		config.optionxform = str
		config.add_section("condor")
		config.set("condor","accounting-group-user",os.popen("whoami").read().strip())
		config.set("condor","universe","vanilla")
		config.add_section("events")
		config.set("events","# Superevent IDs separated by space.")
		config.set("events","superevents","")
		config.add_section("bayeswave")
		config.set("bayeswave","accounting-group","ligo.prod.o3.burst.paramest.bayeswave")
		config.set("bayeswave","request-memory","4000")
		config.set("bayeswave","post-request-memory","6000")
		config.set("bayeswave","bayeswave","/bin/BayesWave")
		config.set("bayeswave","bayeswave_post","/bin/BayesWavePost")
		config.set("bayeswave","megaplot", "/bin/megaplot.py")
		config.set("bayeswave","megasky","/bin/megasky.py")
                config.set("bayeswave","singularity","\"/cvmfs/singularity.opensciencegrid.org/lscsoft/bayeswave:v1.0.5\"")

		with open(opts.get,"w") as f:
			config.write(f)
	elif opts.config is not None:
		import stat
		import numpy as np
		import ast
		import json
		from termcolor import cprint
		from ligo.gracedb.rest import GraceDb
		import glob

		config = ConfigParser()
		config.optionxform = str
		config.read(opts.config)
		# Default sampling rate.
		srate = 2048
		# Maximum seglen and srate
		srate_max = 2048
                seglen_max = 8
		# Directory which stores the PE data.
		pe_dir = "/home/sudarshan.ghonge/Projects/O3_PE/o3a_catalog_events"
		pe_data_dir = "/home/sudarshan.ghonge/Projects/O3a/C01_runs"
		superevents = config["events"]["superevents"].split()

		PWD = os.getcwd()
		# A BayesWave config file template.
		config_bw = ConfigParser()
		config_bw.optionxform = str
		config_bw.add_section("input")
		config_bw.set("input","dataseed","1234")
		config_bw.set("input","seglen","")
		config_bw.set("input","window","1.0")
		config_bw.set("input","flow","20")
		config_bw.set("input","srate",str(srate))
		config_bw.set("input","PSDlength","")
		config_bw.set("input","padding","0.0")
		config_bw.set("input","keep-frac","1.0")
		config_bw.set("input","rho-threshold","7.0")
		config_bw.set("input","ifo-list","")
		config_bw.add_section("engine")
		config_bw.set("engine","bayeswave",config["bayeswave"]["bayeswave"])
		config_bw.set("engine","bayeswave_post",config["bayeswave"]["bayeswave_post"])
		config_bw.set("engine","megaplot",config["bayeswave"]["megaplot"])
		config_bw.set("engine","megasky",config["bayeswave"]["megasky"])
		config_bw.set("engine","singularity",config["bayeswave"]["singularity"])
		config_bw.add_section("datafind")
		config_bw.set("datafind","frtype-list","")
		config_bw.set("datafind","channel-list","")
		config_bw.set("datafind","url-type","file")
		config_bw.set("datafind","veto-categories","[1]")
		config_bw.add_section("bayeswave_options")
		config_bw.set("bayeswave_options","updateGeocenterPSD","")
		config_bw.set("bayeswave_options","waveletPrior","")
		config_bw.set("bayeswave_options","Dmax","100")
		config_bw.set("bayeswave_options","signalOnly","")
		config_bw.add_section("bayeswave_post_options")
		config_bw.set("bayeswave_post_options","0noise","")
		#config_bw.set("bayeswave_post_options","lite","")
		config_bw.add_section("injections")
		config_bw.set("injections","events","all")
		config_bw.add_section("condor")
		config_bw.set("condor","universe",config["condor"]["universe"])
		config_bw.set("condor","checkpoint","")
		config_bw.set("condor","bayeswave-request-memory",config["bayeswave"]["request-memory"])
		config_bw.set("condor","bayeswave_post-request-memory",config["bayeswave"]["post-request-memory"])
		config_bw.set("condor","datafind","/usr/bin/gw_data_find")
		config_bw.set("condor","ligolw_print","/usr/bin/ligolw_print")
		config_bw.set("condor","segfind","/usr/bin/ligolw_segment_query_dqsegdb")
		config_bw.set("condor","accounting-group",config["bayeswave"]["accounting-group"])
		config_bw.set("condor","accounting-group-user",os.popen("whoami").read().strip())
		config_bw.add_section("segfind")
		config_bw.set("segfind","segment-url","https://segments.ligo.org")
		config_bw.add_section("segments")
		config_bw.set("segments","h1-analyze","H1:DMT-ANALYSIS_READY:1")
		config_bw.set("segments","l1-analyze","L1:DMT-ANALYSIS_READY:1")
		config_bw.set("segments","v1-analyze","V1:ITF_SCIENCEMODE")

		# A bash script to execute all runs.
		bw_bash_fp = open("bayeswave_run.sh","w")
		bw_bash_fp.write("#!/bin/bash\n\n")
		client = GraceDb()
		for event in superevents:
			cprint("Processing %s..."%event,"yellow")
			# Create directory.
			if not os.path.exists(event):
		 			os.mkdir(event)
			
			pe_config_file  = glob.glob(os.path.join(pe_dir, event, 'C01_offline', 'Prod0*.ini'))
                        config_pe = ConfigParser()
                        config_pe.optionxform = str
                        config_pe.read(pe_config_file)
                        ifos = ast.literal_eval(config_pe["analysis"]["ifos"])

                        trigtime_file = glob.glob(os.path.join(pe_dir, event, "C01_offline", "*gps*.txt".format(event)))
                        trigtime = np.asscalar(np.loadtxt(trigtime_file[0]))

                        event_dir = os.path.abspath(event)
                       
                        bwFlags=''
			blFlag=False
                        seglen = config_pe.getfloat("engine","seglen")
                        if seglen > seglen_max:
                                cprint("WARNING: seglen (%s) in PE config is greater than the default maximum (%s). seglen is set to the default maximum value."%(seglen,seglen_max),"red")
        			seglen = seglen_max
				blFlag = True
                        srate = config_pe.getfloat("engine","srate")
                        if srate>srate_max:
                                cprint("WARNING: srate (%s) in PE config is greater than the default maximum (%s). srate is set to the default maximum value."%(srate,srate_max),"red")
                                srate = srate_max
				blFlag = True
			if(blFlag):
				bwFlag = '--bayesline-median-psd'
			else:
				extra_files = ''
                                psd_files = {}
                                for ifo in ifos:
                                        psd_dir=os.path.join(pe_data_dir, event, 'PSD')
                                        extra_files = extra_files + os.path.abspath(os.path.join(psd_dir, '{}_PSD.dat'.format(ifo))) +  ','
                                        psd_files[ifo] = '{}_PSD.dat'.format(ifo)
                                        config_bw.set("datafind", "psd-files", str(psd_files))
                                        config_bw.set("condor", "extra-files", str(extra_files.rstrip(',')))
				
                        flows = ast.literal_eval(config_pe["lalinference"]["flow"])
                        flows = {ifo:float(flows[ifo]) for ifo in ifos}
                        flow = np.max(flows.values())
                        frames = ast.literal_eval(config_pe["datafind"]["types"])
                        frames = {ifo:frames[ifo] for ifo in ifos}

                        channels = ast.literal_eval(config_pe["data"]["channels"])
                        channels = {ifo:channels[ifo] for ifo in ifos}
                        channels_arr = [channels[ifo] for ifo in ifos]

                        config_bw.set("input","seglen",str(seglen))
                        config_bw.set("input","ifo-list",str(ifos))
                        config_bw.set("input","PSDlength",str(seglen))
                        config_bw.set("datafind","frtype-list",str(frames))
                        config_bw.set("datafind","channel-list",str(channels))
                        config_bw.set("input","srate",str(srate))
                        config_bw.set("input", "flow", str(flow))		
                                                
                        if(seglen*srate>2048.0*4.0):
                                config_bw.set("condor","bayeswave-request-memory",str(float(config["bayeswave"]["request-memory"])*seglen*srate/(2048.*4.0)))
                                config_bw.set("condor","bayeswave_post-request-memory",str(float(config["bayeswave"]["post-request-memory"])*seglen*srate/(2048.*4.0)))        
			# Get frame types and channels.
			config_event_path = os.path.join(PWD,event,"bayeswave_onsource_config.ini")
			bash_onsource_event_path = os.path.join(PWD,event,"%s_onsource_run.sh"%event)
			with open(config_event_path,"w") as f:
				config_bw.write(f)
			# Write bash script.
			with open(bash_onsource_event_path,"w") as f:
				f.write(bw_bash_template.substitute(workdir=os.path.join(PWD,event,"%s_onsource_run"%event),args="--trigger-time %.6f"%trigtime,config=config_event_path, bwflags=bwFlags, rundir='%s_onsource_run'%event ))
			os.chmod(bash_onsource_event_path,0o744)
			bw_bash_fp.write(". %s\n"%bash_onsource_event_path)
		bw_bash_fp.close()
		os.chmod("bayeswave_run.sh",0o744)
		if opts.submit:
			os.system("./bayeswave_run.sh")
