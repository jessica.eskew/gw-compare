##################################################################################
# Copyright (C) 2020 Isaac Chun Fung Wong
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
##################################################################################

##
# @file gwcomp_generate_injection_table_from_samples.py
# @author Isaac Chun Fung Wong
# @brief This program generates an injection table from the posterior samples of an event.

from __future__ import print_function
import argparse
from string import Template

def parser():
	parser = argparse.ArgumentParser(description="This program generates an injection table from the posterior samples of an event.")
	parser.add_argument("--pefile",type=str,help="Path to PE file",required=True)
        parser.add_argument("--configfile", type=str, help="Path to config file", required=True)
	parser.add_argument("--trigtime-list",type=str,help="A file storing the trigger times.",required=True)
	parser.add_argument("--inj-type",type=str,help="Injection type. Supported: \"maxL\", \"fair-draw\".",required=True)
	parser.add_argument("--root",type=str,help="Root of name of output files.",required=True)
	return parser

def main(args=None):
	import ast
	import os
	import numpy as np
	import pandas as pd
	from glue.ligolw import ligolw
	from glue.ligolw import lsctables
	from glue.ligolw import ilwd
        from configparser import ConfigParser

	opts = parser().parse_args(args)
	if not opts.inj_type in ["maxL","fair-draw"]:
		raise Exception("inj-type (%s) is not supported."%opts.inj_type)
	# Create a datatype for all relavent fields to be filled in the sim_inspiral table
	sim_inspiral_dt = [
		('waveform','|S64'),
		('taper','|S64'),
		('f_lower', 'f8'),
		('mchirp', 'f8'),
		('eta', 'f8'),
		('mass1', 'f8'),
		('mass2', 'f8'),
		('geocent_end_time', 'f8'),
		('geocent_end_time_ns', 'f8'),
		('distance', 'f8'),
		('longitude', 'f8'),
		('latitude', 'f8'),
		('inclination', 'f8'),
		('coa_phase', 'f8'),
		('polarization', 'f8'),
		('spin1x', 'f8'),
		('spin1y', 'f8'),
		('spin1z', 'f8'),
		('spin2x', 'f8'),
		('spin2y', 'f8'),
		('spin2z', 'f8'),
		('amp_order', 'i4'),
		('numrel_data','|S64')
	]
	trigtimes = np.loadtxt(opts.trigtime_list)
	ninj = trigtimes.shape[0]
	injections = np.zeros((ninj),dtype=sim_inspiral_dt)
        pe_structured_array = np.genfromtxt(opts.pefile, names=True)
	params = pe_structured_array.dtype.names
	post_samples = pd.DataFrame(pe_structured_array)
	if opts.inj_type=="maxL":
		post_samples = pd.DataFrame(np.repeat(np.array(post_samples[post_samples["log_likelihood"]==post_samples["log_likelihood"].max()]),ninj,axis=0),columns=params)
	else:
		post_samples = post_samples.sample(ninj)
	config = ConfigParser()
        config.optionxform = str
        config.read(opts.configfile)
	# Get approximant.
	approx = config["engine"]["approx"]
	amp_order = int(config["engine"]["amporder"])
	flow = ast.literal_eval(config["lalinference"]["flow"])
	flow = flow[flow.keys()[0]]
	trigtimes_ns,trigtimes_s = np.modf(trigtimes)
	trigtimes_ns *= 10**9
	injections["waveform"] = [approx for i in range(ninj)]
	injections["taper"] = ["TAPER_NONE" for i in range(ninj)]
	injections["f_lower"] = [flow for i in range(ninj)]
	try:
		injections["mchirp"] = np.array(post_samples["chirp_mass"])
        except:
		injections["mchirp"] = np.array(post_samples["mc"])
	try:
		injections["eta"] = np.array(post_samples["symmetric_mass_ratio"])
 	except:
		injections["eta"] = np.array(post_samples["eta"])
	try:
		injections["mass1"] = np.array(post_samples["mass_1"])
	except:
		injections["mass1"] = np.array(post_samples["m1"])
        try:
                injections["mass2"] = np.array(post_samples["mass_2"])
        except:
                injections["mass2"] = np.array(post_samples["m2"])
	injections["geocent_end_time"] = trigtimes_s
	injections["geocent_end_time_ns"] = trigtimes_ns
	try:
		injections["distance"] = np.array(post_samples["luminosity_distance"])
	except:
		injections["distance"] = np.array(post_samples["dist"])
	injections["longitude"] = np.array(post_samples["ra"])
	injections["latitude"] = np.array(post_samples["dec"])
	injections["inclination"] = np.array(post_samples["iota"])
	injections["coa_phase"] = np.array(post_samples["phase"])
	injections["polarization"] = np.array(post_samples["psi"])
	try:
		injections["spin1x"] = np.array(post_samples["spin_1x"])
	except:
		injections["spin1x"] = np.array(post_samples["a1"])*np.sin(np.array(post_samples["theta1"]))*np.cos(np.array(post_samples["phi1"]))
	try:
		injections["spin1y"] = np.array(post_samples["spin_1y"])
	except:
                injections["spin1y"] = np.array(post_samples["a1"])*np.sin(np.array(post_samples["theta1"]))*np.sin(np.array(post_samples["phi1"]))
        try:
		injections["spin1z"] = np.array(post_samples["spin_1z"])
	except:
                injections["spin1z"] = np.array(post_samples["a1"])*np.cos(np.array(post_samples["theta1"]))
	try:
		injections["spin2x"] = np.array(post_samples["spin_2x"])
	except:
                injections["spin2x"] = np.array(post_samples["a2"])*np.sin(np.array(post_samples["theta2"]))*np.cos(np.array(post_samples["phi2"]))
	try:
		injections["spin2y"] = np.array(post_samples["spin_2y"])
	except:
                injections["spin2y"] = np.array(post_samples["a2"])*np.sin(np.array(post_samples["theta2"]))*np.sin(np.array(post_samples["phi2"]))
	try:
		injections["spin2z"] = np.array(post_samples["spin_2z"])
	except:
                injections["spin2z"] = np.array(post_samples["a2"])*np.cos(np.array(post_samples["theta2"]))
	injections["amp_order"] = [amp_order for i in range(ninj)]
	injections["numrel_data"] = ["" for i in range(ninj)]
	# Create a new XML document
	xmldoc = ligolw.Document()
	xmldoc.appendChild(ligolw.LIGO_LW())
	sim_table = lsctables.New(lsctables.SimInspiralTable)
	xmldoc.childNodes[0].appendChild(sim_table)

	# Add empty rows to the sim_inspiral table
	for inj in range(ninj):
		row = sim_table.RowType()
		for slot in row.__slots__:
			setattr(row, slot, 0)
		sim_table.append(row)

	# Fill in IDs
	for i,row in enumerate(sim_table):
		row.process_id = ilwd.ilwdchar("process:process_id:{0:d}".format(i))
		row.simulation_id = ilwd.ilwdchar("sim_inspiral:simulation_id:{0:d}".format(i))

	# Fill rows
	for field in injections.dtype.names:
		vals = injections[field]
		for row, val in zip(sim_table, vals):
			setattr(row, field, val)
	with open(opts.root+".xml","w") as f:
		xmldoc.write(f)
